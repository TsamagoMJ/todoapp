import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';
import { TodoService } from '../todo/shared/todo.service';

// /Users/apple/Documents/todoList/src/app/todo/shared/todo.service.ts

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
 posts$: Post[];

  constructor( private postService: TodoService) { }

  ngOnInit() {
    return this.postService.getposts().subscribe(data => this.posts$ = data);
  }

}
