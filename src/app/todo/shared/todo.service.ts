import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { stringify } from 'querystring';
import { HttpClient } from '@angular/common/http';
import { Post } from 'src/app/post.model';


@Injectable({
  providedIn: 'root'
})
export class TodoService {
 apiUr = 'https://jsonplaceholder.typicode.com/posts';
  toDoList: AngularFireList<any>

  constructor( private firebasedb : AngularFireDatabase, private _http: HttpClient) { }

//Get Posts
  getposts() {
  return this._http.get<Post[]>(this.apiUr)
  }

//Get to do list
  getToDoList() {
  this.toDoList = this.firebasedb.list("titles");
  return this.toDoList;
  }


 addTitle(title:string) {
   this.toDoList.push({
     title:title,
     isChecked:false
   });
  }
  checkOrUncheckTitle($key:string, flag:boolean) {
 this.toDoList.update($key, {isChecked:flag})
  }

 removeTitle($key:string) {
   this.toDoList.remove($key);
 }


 }



