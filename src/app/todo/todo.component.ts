import { Component, OnInit } from '@angular/core';
import { TodoService } from './shared/todo.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
  providers: [TodoService]
})
export class TodoComponent implements OnInit {
  toDoListArray:any [];
  constructor( private toDoService: TodoService, private _router: Router) { }

  ngOnInit() {

    this.toDoService.getToDoList().snapshotChanges().subscribe(item => {
      this.toDoListArray = [];
      item.forEach(element => {
        var x = element.payload.toJSON();
        x["$key"] = element.key;
        this.toDoListArray.push(x);
      })

      //sort array isChecked false -> true
      this.toDoListArray.sort((a,b) => {
        return a.ischecked - b.isChecled;
      })
    });

  }


onAdd(itemTitle) {
  this.toDoService.addTitle(itemTitle.value);
  itemTitle.value = null;
}

alterCheck($key: string,isChecked) {
  this.toDoService.checkOrUncheckTitle($key,!isChecked);
}

onDelete($key : string){
  this.toDoService.removeTitle($key);
}

}
