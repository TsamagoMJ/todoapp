import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as rxjs from 'rxjs';
import { HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
 

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment} from '../environments/environment';
import { TodoComponent } from './todo/todo.component';
import { PostComponent } from './post/post.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { TodoService } from './todo/shared/todo.service';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    PostComponent,
    HomeComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
