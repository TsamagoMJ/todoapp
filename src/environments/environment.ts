// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBj0SdparO2l2C_hKpSV9KveTIhdfqpdk4",
    authDomain: "todolist-1070a.firebaseapp.com",
    databaseURL: "https://todolist-1070a.firebaseio.com",
    projectId: "todolist-1070a",
    storageBucket: "todolist-1070a.appspot.com",
    messagingSenderId: "531418276385",
    appId: "1:531418276385:web:5de56c37eadcc924f23960",
    measurementId: "G-2EKTF69B6B"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
